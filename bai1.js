function kiemTraSoNguyenTo(n) {
  var soNguyenTo = true;
  if (n < 2) {
    soNguyenTo = false;
  } else if (n == 2) {
    soNguyenTo = true;
  } else if (n % 2 == 0) {
    soNguyenTo = false;
  } else {
    for (var i = 3; i <= Math.sqrt(n); i += 2) {
      if (n % i == 0) {
        soNguyenTo = false;
        break;
      }
    }
  }
  return soNguyenTo;
}

function soNguyenTo() {
  var sotunhienValue = document.getElementById("txt_so_n").value * 1;
  sotunhienValue = parseInt(sotunhienValue);
  var contentHTML = "";
  for (i = 1; i <= sotunhienValue; i++) {
    if (kiemTraSoNguyenTo(i)) {
      contentHTML += i + "<br>";
    }
  }
  document.getElementById("ket_qua").innerHTML = `${contentHTML}`;
}
